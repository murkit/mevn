const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const http = require('http');

//настройки подключение к бд

mongoose.connect('mongodb://localhost:27017/mevnshop', {
    useCreateIndex: true,
    useNewUrlParser: true,

});

//инициализируем приложение
const app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

//объявим наши роуты
const PORT = 3000;
http.createServer({}.app).listen(PORT);
console.log(`Server running ut ${PORT}`);